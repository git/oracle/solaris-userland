OpenSSL 1.0.2 will be removed from a future version of Oracle Solaris.
The replacement is OpenSSL 3.x already delivered with the system.
